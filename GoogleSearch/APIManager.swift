//
//  APIManager.swift
//  GoogleSearch
//
//  Created by Igor Nikolaev on 7/4/16.
//  Copyright © 2016 Igor Nikolaev. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireSwiftyJSON

class APIManager: NSObject {

    let API_KEY: String! = "AIzaSyBi4iBNKDi05psb7XfRrQcpq6_wyNQBmaA"
    let API_URL: String! = "https://maps.googleapis.com/maps/api/place/textsearch/json"
    
    var searchRequest: Alamofire.Request?
    
    typealias CompletionBlock = (results: NSArray!, error: NSError!) -> Void
    
    static let sharedInstance = APIManager()
    
    private override init() {
        
    }
    
    // MARK 
    
    func fetchGoogleResults(query: String, completion: CompletionBlock) {
        self.searchRequest?.cancel()
        
        let urlString: String! = API_URL
        self.searchRequest = Alamofire.request(.GET, urlString, parameters: ["query": query, "key" : API_KEY])
            .validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"])
            .responseJSON { response in

                print(response.request)
                print(response.response)

                let statusCode = response.response?.statusCode
                if statusCode == 200
                {
                    if let JSON: NSDictionary = (response.result.value as! NSDictionary) {
                        
                        print("JSON: \(JSON)")
                        
                        if let error: NSError! = self.validateResponse(JSON) {
                            completion(results: nil, error: error)
                        } else {
                            let fetchedStringsResults: NSMutableArray = []
                            if let results = JSON["results"] as? NSArray {
                                for result:NSDictionary in results as! [NSDictionary] {
                                    if let addressString = result["formatted_address"] as? String {
                                        fetchedStringsResults.addObject(addressString)
                                    }
                                }
                                completion(results: fetchedStringsResults, error: nil)
                            }
                        }
                    }
                }
                else
                {
                    if response.result.error?.code != 999  //canceled
                    {
                        completion(results: nil, error: response.result.error!)
                    }
                }
        }
    }
    
    // MARK: Response JSON validating for error message
    
    func validateResponse(result: NSDictionary) -> NSError! {
        if let errorMsg = result["error_message"] as? String {
            let userInfo: [NSObject : AnyObject] =
                [
                    NSLocalizedDescriptionKey :  NSLocalizedString("Unauthorized", value: errorMsg, comment: ""),
                    NSLocalizedFailureReasonErrorKey : NSLocalizedString("Unauthorized", value: errorMsg, comment: "")
            ]
            let err = NSError(domain: "Domain", code: 401, userInfo: userInfo)
            return err
        } else {
            return nil
        }
    }
    
}

