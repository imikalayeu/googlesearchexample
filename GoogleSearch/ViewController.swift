//
//  ViewController.swift
//  GoogleSearch
//
//  Created by Igor Nikolaev on 6/28/16.
//  Copyright © 2016 Igor Nikolaev. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate {

    @IBOutlet weak var contentTable: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!

    var shownResults = [String]()
    let disposeBag = DisposeBag() 
    
    // MARK: UIView life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        searchBar
            .rx_text
            .throttle(0.5, scheduler: MainScheduler.instance)
            .distinctUntilChanged()
            .subscribeNext { [unowned self] (query) in
                let stringQuery: String! = query
                if stringQuery.characters.count == 0 {
                    self.shownResults = []
                    self.contentTable.reloadData()
                    return;
                }
                APIManager.sharedInstance.fetchGoogleResults(query, completion: { (results, error) in
                    if error == nil {
                        self.shownResults = results as! [String]
                        self.contentTable.reloadData()
                    } else {
                        self.displayErrorMsg(error.localizedDescription)
                    }
                })
            }
            .addDisposableTo(disposeBag)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // MARK: UITableViewDataSource
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return shownResults.count
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 44.0
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier(SearchResultCell.cellIdentifier(), forIndexPath: indexPath) as! SearchResultCell
        cell.resultLabel?.text = shownResults[indexPath.row]
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
    }

    //MARK: UISearchBarDelegate
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        self.searchBar.resignFirstResponder()
    }
    

    // MARK: UIHelpers
    
    func displayErrorMsg(msg: String) {
        let alertController = UIAlertController(title: "Error", message: msg, preferredStyle: .Alert)
        let OKAction = UIAlertAction(title: "OK", style: .Default) { (action) in
        }
        alertController.addAction(OKAction)
        self.presentViewController(alertController, animated: true) {
        }
    }
}

