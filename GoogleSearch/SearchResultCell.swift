//
//  SearchResultCell.swift
//  GoogleSearch
//
//  Created by Igor Nikolaev on 7/4/16.
//  Copyright © 2016 Igor Nikolaev. All rights reserved.
//

import UIKit

class SearchResultCell: UITableViewCell {

    @IBOutlet weak var resultLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    static func cellIdentifier() -> String {
        return "SearchResultCell"
    }
}
